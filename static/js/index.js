var ws = new WebSocket('wss://' + location.host + '/one2many');
var video;
var webRtcPeer;

window.onload = function() {
	
	video = document.getElementById('video');

	document.getElementById('call').addEventListener('click', function() { presenter(); } );
	document.getElementById('terminate').addEventListener('click', function() { stop(); } );
	document.getElementById('viewer').addEventListener('click', function() { viewer(); } );

	
}
ws.onopen = function()
               {
                  // Web Socket is connected, send data using send()
                  //setTimeout(viewer,2000);
               };
window.onbeforeunload = function() {
	ws.close();
}
function onError(errr){
 console.log('*************'+errr);
}
ws.onmessage = function(message) {
	var parsedMessage = JSON.parse(message.data);
	console.info('Received message: ' + message.data);

	switch (parsedMessage.id) {
	case 'presenterResponse':
		presenterResponse(parsedMessage);
		setTimeout(hideSpinner,1000);
		break;
	case 'viewerResponse':
		viewerResponse(parsedMessage);
		setTimeout(hideSpinner,1000);
		break;
	case 'stopCommunication':
		dispose();
		break;
	case 'iceCandidate':
		webRtcPeer.addIceCandidate(parsedMessage.candidate)
		break;
	default:
		console.error('Unrecognized message', parsedMessage);
	}
}

function presenterResponse(message) {
	if (message.response != 'accepted') {
		var errorMsg = message.message ? message.message : 'Unknow error';
		//console.warn('Call not accepted for the following reason: ' + errorMsg);
		
		dispose();
	} else {
		webRtcPeer.processAnswer(message.sdpAnswer);
	}
}

function viewerResponse(message) {
	if (message.response != 'accepted') {
		var errorMsg = message.message ? message.message : 'Unknow error';
		console.warn('Call not accepted for the following reason: ' + errorMsg);
		setTimeout(dispose,2000);
		alert('Broadcast Not Yet Started ! Please try later');
		
	} else {
		webRtcPeer.processAnswer(message.sdpAnswer);
	}
}

function presenter() {
	if (!webRtcPeer) {
		showSpinner(video);

		var options = {
			localVideo: video,
			onicecandidate : onIceCandidate
	    }

		webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options, function(error) {
			if(error) return onError(error);

			this.generateOffer(onOfferPresenter);
		});
		setTimeout(function mute(){
		   var vid = document.getElementById("video");
           vid.volume = 0.00001;
		},100);
		setTimeout(function mute(){
		   var vid = document.getElementById("video");
           vid.volume = 0.00001;
		},500);
		setTimeout(function mute(){
		   var vid = document.getElementById("video");
           vid.volume = 0.00001;
		},1000);
		setTimeout(function mute(){
		   var vid = document.getElementById("video");
           vid.volume = 0.00001;
		},2000);
		setTimeout(function mute(){
		   var vid = document.getElementById("video");
           vid.volume = 0.00001;
		},4000);
		setTimeout(function mute(){
		   var vid = document.getElementById("video");
           vid.volume = 0.00001;
		},8000);
        
	}
}

function onOfferPresenter(error, offerSdp) {
    if (error) return onError(error);

	var message = {
		id : 'presenter',
		sdpOffer : offerSdp
	};
	sendMessage(message);
}

function viewer() {
	if (!webRtcPeer) {
		showSpinner(video);

		var options = {
			remoteVideo: video,
			onicecandidate : onIceCandidate
		}

		webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options, function(error) {
			if(error) return onError(error);

			this.generateOffer(onOfferViewer);
		});
	}
}

function onOfferViewer(error, offerSdp) {
	if (error) return onError(error)

	var message = {
		id : 'viewer',
		sdpOffer : offerSdp
	}
	sendMessage(message);
}

function onIceCandidate(candidate) {
	   console.log('Local candidate' + JSON.stringify(candidate));

	   var message = {
	      id : 'onIceCandidate',
	      candidate : candidate
	   }
	   sendMessage(message);
}

function stop() {
	if (webRtcPeer) {
		var message = {
				id : 'stop'
		}
		sendMessage(message);
		dispose();
	}
}

function dispose() {
	if (webRtcPeer) {
		webRtcPeer.dispose();
		webRtcPeer = null;
	}
	hideSpinner(video);
	$('#call').show();
   $('#terminate').hide();
   $('#viewer').show();
}

function sendMessage(message) {
	var jsonMessage = JSON.stringify(message);
	console.log('Senging message: ' + jsonMessage);
	ws.send(jsonMessage);
}

function showSpinner() {
	/*for (var i = 0; i < arguments.length; i++) {
		arguments[i].poster = './img/transparent-1px.png';
		arguments[i].style.background = 'center transparent url("./img/spinner.gif") no-repeat';
	}*/
	
	document.getElementById('connecting').style.display = 'block';  
}

function hideSpinner() {
	/*for (var i = 0; i < arguments.length; i++) {
		arguments[i].src = '';
		arguments[i].poster = './img/webrtc.png';
		arguments[i].style.background = '';
	}*/
   $('#call').hide();
   $('#terminate').show();
   document.getElementById('connecting').style.display = 'none';  
   $('#viewer').hide();
}

/**
 * Lightbox utility (to display media pipeline image in a modal dialog)
 */
$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
	event.preventDefault();
	$(this).ekkoLightbox();
});
